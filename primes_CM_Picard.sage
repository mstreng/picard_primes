"""
AUTHORS: Pınar Kılıçer, Elisa Lorenzo García, Marco Streng

This file implements the denominator bounds of

[KLS] Pınar Kılıçer, Elisa Lorenzo García, Marco Streng - Primes dividing invariants of CM Picard curves
[KLLNOS] Pınar Kılıçer, Kristin Lauter, Elisa Lorenzo García, Rachel Newton, Ekin Ozman, Marco Streng - A bound on the primes of bad reduction for CM curves of genus 3

in the case of maximal orders.

As an example/application, it creates the LaTeX source file of Section 10 of loc. cit.

For the denominator multiple of [KLS, Section 9], see real_field_to_Picard_primes (and its documentation) below.
For the number B appearing in the bound of [KLLNOS], seeof [KLS, Section 9], see sextic_CM_to_KLLNOS_B (and its documentation) below.
For creating the LaTeX source file of [KLS, Section 10], see the documentation of create_tex below.
"""

try:
    a = verbosity
except NameError:
    verbosity = 0

P.<x> = QQ[]


def real_field_to_Picard_primes(F, B=None, one_mu=False, remark44=False, output_mu=False):
    """
    The function real_field_to_Picard_primes tries out a few values of mu to and outputs two numbers.
    The second output is the gcd of N_{\mu} as in Section 9 for these values of mu.

    By Theorem 9.1, every prime p > 3 in the denominator divides the second output.

    EXAMPLE::

        sage: attach("computations1.sage")
        sage: F = NumberField(x^3-x^2-3*x+1, 'a') # K = F(zeta_3)
        sage: F.is_totally_real()
        True
        sage: real_field_to_Picard_primes(F)

        (2^10 * 5^2 * 13 * 17 * 19 * 23 * 31 * 37 * 43,
         2^147 * 5^8 * 13^2 * 17^2 * 19^2 * 23 * 31 * 37 * 43)

    So by Theorem 9.1, the primes in the denominator form a subset of
    {2, 3, 5, 13, 17, 19, 23, 31, 37, 43}.

    In more detail, the function gives two outputs. The second is as explained above,
    the first is the same except that for computing N_{\mu} the lcm is used instead of the product.
    It has the same set of prime factors, and may still have a nice interpretation, but our conjecture is about N_{\mu}.

    The input B is used for finding small values of mu. For details, see real_field_to_mu. Safely keep it None to let the
    code figure it out for itself.

    To speed things up, you can take one_mu to be True. As far as we know, this may yield worse results, but it seems
    to be not so much worse than keeping one_mu=False.

    EXAMPLE::

        sage: real_field_to_Picard_primes(F, one_mu=True)
        (2^10 * 5^2 * 13 * 17 * 19 * 23 * 31 * 37 * 43,
         2^147 * 5^8 * 13^2 * 17^2 * 19^2 * 23 * 31 * 37 * 43)
    
    This code by default does not take into account the improvement of Remark 4.4. To do so, set remark44 to be True.
    
    EXAMPLE::

        sage: real_field_to_Picard_primes(F, one_mu=True, remark44=True)
        (2^8 * 5^2 * 13 * 17 * 19 * 23 * 31 * 37 * 43,
         2^99 * 5^8 * 13^2 * 17^2 * 19^2 * 23 * 31 * 37 * 43)

    To also get the list of values mu as output, use output_mu=True.

    EXAMPLE::
    
        sage: real_field_to_Picard_primes(F, output_mu=True)
        ([-2*a + 1,
          2*a - 1,
          2*a,
          -2*a,
          -2*a^2 + 2*a + 4,
          2*a^2 - 2*a - 4,
          -2*a + 2,
          2*a - 2,
          -2*a^2 + 2*a + 3,
          2*a^2 - 2*a - 3,
          -2*a^2 + 2*a + 5,
          2*a^2 - 2*a - 5,
          2*a + 1,
          -2*a - 1,
          -2*a^2 + 5,
          2*a^2 - 5,
          -2*a + 3,
          2*a - 3,
          -2*a^2 + 4,
          2*a^2 - 4,
          -2*a^2 + 2*a + 2,
          2*a^2 - 2*a - 2,
          -2*a^2 + 2*a + 6,
          2*a^2 - 2*a - 6],
         2^10 * 5^2 * 13 * 17 * 19 * 23 * 31 * 37 * 43,
         2^147 * 5^8 * 13^2 * 17^2 * 19^2 * 23 * 31 * 37 * 43)


    Here is some information on the inner workings of this function.
    For each mu, we have to take all (x, a) in the correct range, but then we eliminate
    some (x,a) by
     * computing iota(b) where b ranges over a basis of O_K and throwing away those
       for which the matrix is not integral enough,
     * computing alpha, gamma, alpha*gamma-beta^2 and checking whether it is positive.
    Then for each (x, a), we compute n. The actual denominator divides one of these n.
    So we compute the lcm of n.

    In previous versions, it looked like it did not help to use multiple values of
    mu, so to save time, one can choose to use only one value of mu.
    When using multiple values of mu, note that Gal(F/Q) x {-1, 1} makes sure that every
    mu essentially occurs 6 times, so the code is 6 times as slow as it could be.
    """
    list_mu = real_field_to_mu(F, B=B)
    if one_mu:
        list_mu = list_mu[:1]
        assert len(list_mu) == 1

    l = [n_from_mu(mu, remark44=remark44) for mu in list_mu]
    out = gcd([a for (a,b) in l]).factor()
    outprod = gcd([b for (a,b) in l]).factor()
    if output_mu:
        return list_mu, out, outprod

    return out, outprod


def sextic_CM_to_KLLNOS_B(K, bar=None):
    """
    Given a CM field K, returns the the minimal tr(x*xbar)/2 for x running
    over O_K \cap i*RR.

    This is the minimal number B in KLLNOS.

    EXAMPLES::

        sage: attach("computations1.sage")
        sage: p = x^3+2*x^2-21*x-27
        sage: K0.<a> = NumberField(p)
        sage: K1.<b> = K0.extension(x^2+67)
        sage: K.<c> = K1.absolute_field()
        sage: [bar] = [phi for phi in K.automorphisms() if phi(c) != c and phi(phi(c)) == c]
        sage: sextic_CM_to_KLLNOS_B(K, bar)
        (67,
         -167/1348509*c^5 + 4/6709*c^4 - 16828/1348509*c^3 + 123839/1348509*c^2 - 1563818/1348509*c + 7185422/1348509,
         x^6 + 67*x^4 + 1206*x^2 + 5427)

    """
    if bar is None:
        bar = K.complex_conjugation()
    B = 1
    while True:
        l = elements_of_bounded_trace_norm(K.maximal_order().basis(), B, bar=bar)
        l = [a for a in l if bar(a) == -a and not a^2 in QQ]
        if len(l) > 0:
            return -(l[0]^2).trace()/2, l[0], l[0].minpoly()
        B = B + 1


# Here are the fields K+ and the Picard curve coefficients of conjectural CM curves computed
# by Koike-Weng and Lario-Somoza. For an explanation of what these entries mean, see the
# docstring of compute_bounds.
poly_and_invars = [
( "2", "4.1.2", x^3-x^2-2*x+1, [(-7^2*2, 7^2*2^3, -7^3)]),
( "3", "4.1.3", x^3-x^2-4*x-1, [(-13*2*7^2, 2^3*13*5*47, -5^2*31*13^2)]),
( "4", "4.1.4", x^3+x^2-10*x-8, [(-73*7*2*31, 2^11*47*31, -7*31^2*11593)]),
( "5", "4.1.5", x^3-x^2-14*x-8, [(-2*7*223*43^2, 2^7*11*41*43^2*59, -11^2*43^3*419*431)]),
( None, "4.2.1.1", x^3-21*x-28, [(-2*3^2*5^2*7^2, 2^9*7^2*71, -3^2*5*7^3*2621)]),
( None, "4.2.1.2", x^3-21*x-35, [(-2^2*3^2*7^2*37,5*7^2*149*257,-2*3^2*5^2*7^3*2683)]),
( None, "4.2.1.3", x^3-39*x-26, [(-2*3^2*5^2*7*11*13,2^7*11*13*59*149,-3^2*5*7*13^2*17*17669)]),
( None, "4.3.1 (corrected w.r.t.~arXiv version 1)", x^3-61*x-183, [
    (-1 * 2 * 3 * 7 * 61^2 * 1289, 2^3 * 3^7 * 11 * 41 * 53 * 61^2, -1 * 3^2 * 7 * 11^2 * 61^3 * 419 * 4663),
    ([-4059720,-547484, 89264], [772138494, 49526073, -29558196], [-72348132021, -16281030326, 88325678])]),
( None, "4.3.2", x^3-x^2-22*x-5, [(2*7*67*179, 2^3*3^3*5*67*137, 5^2*7*67^2*71*89),
    ([-1290744, -263088, 12222], [1277237160, 232016400, -19721880], [-447679991475, -62791404525, 11453819175])
                                  ])
        ]


def create_tex(l, use_ceil=False):
    """
    If l is the output of compute_bounds, then this file creates the LaTeX code of [KLS, Section 10].

    Here is how to create the LaTeX file for Section 10. We include the output it gave us with SageMath 8.0::

        sage: attach("computations1.sage")
        sage: l = compute_bounds() # takes a long time (multiple minutes)
        sage: l

        [('2',
          '4.1.2',
          x^3 - x^2 - 2*x + 1,
          [(-98, 392, -343)],
          '-2 \\alpha^{2} + 3',
          2^8 * 7 * 13,
          2^84 * 7^3 * 13^3,
          2^6 * 7 * 13,
          2^54 * 7^3 * 13^3,
          15),
         ('3',
          '4.1.3',
          x^3 - x^2 - 4*x - 1,
          [(-1274, 24440, -130975)],
          '-2 \\alpha^{2} + 2 \\alpha + 5',
          2^8 * 5^2 * 13 * 31 * 47,
          2^153 * 5^18 * 13^3 * 31^3 * 47^3,
          2^6 * 5^2 * 13 * 31 * 47,
          2^99 * 5^18 * 13^3 * 31^3 * 47^3,
          27),
         ('4',
          '4.1.4',
          x^3 + x^2 - 10*x - 8,
          [(-31682, 2983936, -77986111)],
          '-\\alpha^{2} + \\alpha + 7',
          2^15 * 23 * 29 * 31 * 47 * 61 * 89 * 101 * 139,
          2^615 * 23^6 * 29^6 * 31^3 * 47^6 * 61^6 * 89^3 * 101^3 * 139^3,
          2^13 * 23 * 29 * 31 * 47 * 61 * 89 * 101 * 139,
          2^495 * 23^6 * 29^6 * 31^3 * 47^6 * 61^6 * 89^3 * 101^3 * 139^3,
          63),
         ('5',
          '4.1.5',
          x^3 - x^2 - 14*x - 8,
          [(-5772578, 6297605248, -1737328844383)],
          '-2 \\alpha + 1',
          2^16 * 11^2 * 41 * 43 * 47 * 59 * 97 * 131 * 173 * 211 * 223 * 269,
          2^864 * 11^27 * 41^9 * 43^3 * 47^6 * 59^9 * 97^3 * 131^3 * 173^3 * 211^3 * 223^3 * 269^3,
          2^14 * 11^2 * 41 * 43 * 47 * 59 * 97 * 131 * 173 * 211 * 223 * 269,
          2^690 * 11^27 * 41^9 * 43^3 * 47^6 * 59^9 * 97^3 * 131^3 * 173^3 * 211^3 * 223^3 * 269^3,
          87),
         (None,
          '4.2.1.1',
          x^3 - 21*x - 28,
          [(-22050, 1781248, -40455135)],
          '2 \\alpha',
          2^15 * 3^2 * 7^2 * 31 * 47 * 59 * 61 * 71 * 173,
          2^433 * 3^55 * 7^11 * 31^3 * 47^3 * 59^3 * 61^3 * 71^3 * 173^3,
          2^13 * 3^2 * 7^2 * 31 * 47 * 59 * 61 * 71 * 173,
          2^347 * 3^55 * 7^11 * 31^3 * 47^3 * 59^3 * 61^3 * 71^3 * 173^3,
          42),
         (None,
          '4.2.1.2',
          x^3 - 21*x - 35,
          [(-65268, 9381785, -414121050)],
          '-2 \\alpha^{2} + 4 \\alpha + 28',
          2^8 * 3^2 * 5^2 * 7^2 * 11^2 * 23 * 71 * 149 * 257,
          2^245 * 3^58 * 5^36 * 7^8 * 11^12 * 23^6 * 71^3 * 149^3 * 257^3,
          2^6 * 3^2 * 5^2 * 7^2 * 11^2 * 23 * 71 * 149 * 257,
          2^159 * 3^58 * 5^36 * 7^8 * 11^12 * 23^6 * 71^3 * 149^3 * 257^3,
          42),
         (None,
          '4.2.1.3',
          x^3 - 39*x - 26,
          [(-450450, 160910464, -15990356655)],
          '-\\frac{1}{2} \\alpha^{2} + \\frac{3}{2} \\alpha + 13',
          2^19 * 3^2 * 11^2 * 13^2 * 29 * 53 * 59 * 109 * 113 * 149 * 233 * 359 * 467 * 541 * 577,
          2^921 * 3^100 * 11^21 * 13^8 * 29^6 * 53^3 * 59^6 * 109^3 * 113^3 * 149^6 * 233^3 * 359^3 * 467^3 * 541^3 * 577^3,
          2^17 * 3^2 * 11^2 * 13^2 * 29 * 53 * 59 * 109 * 113 * 149 * 233 * 359 * 467 * 541 * 577,
          2^769 * 3^100 * 11^21 * 13^8 * 29^6 * 53^3 * 59^6 * 109^3 * 113^3 * 149^6 * 233^3 * 359^3 * 467^3 * 541^3 * 577^3,
          78),
         (None,
          '4.3.1 (corrected w.r.t.~arXiv version 1)',
          x^3 - 61*x - 183,
          [(-201447498, 1556147830248, -3380608376440911),
           ([-4059720, -547484, 89264],
            [772138494, 49526073, -29558196],
            [-72348132021, -16281030326, 88325678])],
          '-4 \\alpha^{2} + 18 \\alpha + 163',
          2^8 * 3^8 * 11^2 * 23 * 37 * 41 * 53 * 61 * 89 * 113 * 131 * 149 * 191 * 367 * 613 * 643 * 733 * 907,
          2^705 * 3^444 * 11^36 * 23^18 * 37^9 * 41^9 * 53^9 * 61^3 * 89^3 * 113^3 * 131^3 * 149^3 * 191^3 * 367^3 * 613^3 * 643^3 * 733^3 * 907^3,
          2^6 * 3^8 * 11^2 * 23 * 37 * 41 * 53 * 61 * 89 * 113 * 131 * 149 * 191 * 367 * 613 * 643 * 733 * 907,
          2^459 * 3^444 * 11^36 * 23^18 * 37^9 * 41^9 * 53^9 * 61^3 * 89^3 * 113^3 * 131^3 * 149^3 * 191^3 * 367^3 * 613^3 * 643^3 * 733^3 * 907^3,
          123),
         (None,
          '4.3.2',
          x^3 - x^2 - 22*x - 5,
          [(167902, 9913320, 4964048425),
           ([-1290744, -263088, 12222],
            [1277237160, 232016400, -19721880],
            [-447679991475, -62791404525, 11453819175])],
          '-\\frac{2}{3} \\alpha^{2} + \\frac{29}{3}',
          2^8 * 3^7 * 5^3 * 43 * 53 * 59 * 67 * 89 * 107 * 109 * 137 * 149 * 179 * 223 * 241 * 263 * 269 * 397 * 643 * 997 * 1087,
          2^759 * 3^465 * 5^96 * 43^6 * 53^9 * 59^6 * 67^3 * 89^6 * 107^3 * 109^3 * 137^3 * 149^3 * 179^6 * 223^3 * 241^3 * 263^3 * 269^3 * 397^3 * 643^3 * 997^3 * 1087^3,
          2^6 * 3^7 * 5^3 * 43 * 53 * 59 * 67 * 89 * 107 * 109 * 137 * 149 * 179 * 223 * 241 * 263 * 269 * 397 * 643 * 997 * 1087,
          2^495 * 3^465 * 5^96 * 43^6 * 53^9 * 59^6 * 67^3 * 89^6 * 107^3 * 109^3 * 137^3 * 149^3 * 179^6 * 223^3 * 241^3 * 263^3 * 269^3 * 397^3 * 643^3 * 997^3 * 1087^3,
          135)]

        sage: s = create_tex(l, False)
        sage: f = open('examples.tex', 'w')
        sage: f.write(s)
        sage: f.close()

        Actual minimal denominator:  2^3
        2^30

        Actual minimal denominator:  2^3 * 5 * 47
        2^54

        Actual minimal denominator:  2^11 * 47
        2^120

        Actual minimal denominator:  2^7 * 11 * 41 * 59
        2^174

        Actual minimal denominator:  2^9 * 71
        2^86

        Actual minimal denominator:  5 * 149 * 257
        2^86

        Actual minimal denominator:  2^7 * 11 * 59 * 149
        2^152

    """
    if use_ceil:
        my_ceil = ceil
    else:
        my_ceil = lambda x: x

    texout = "% Don't edit this file!!! It is automatically generated (and overwritten) by computations1.sage\n\n"

    casecount = 0

    firstexample = True
    firstlongexample = True

    for (KW, LS, pol, i, mu, N, Nprod, Nremark44, Nremark44prod, B) in l:
        casecount = casecount + 1

        texout = texout + "\\textbf{Example " + str(casecount) + ".}\n"
        texout = texout + "For the field $K_+=\\Q(\\alpha)=\\Q[x]/(" + (pol)._latex_() + ")$,\n"
        K0.<alpha> = NumberField(pol)
        P.<X> = K0[]
        Krel.<beta> = NumberField(X^2+3)
        K.<gamma> = Krel.absolute_field()
        hstar = K.class_number() / K0.class_number()

        texout = texout + "let $K = K_+(\zeta_3)$. Then there \n"
        if hstar == 1:
            if len(i) != 1:
                print hstar, i
                texout = texout + str(hstar) + "," + str(i) + "???"
            texout = texout + "is exactly one curve "
        elif hstar == 4:
            assert len(i) == 2
            texout = texout + "are exactly four curves "
        else:
            raise NotImplementedError
        if firstexample:
            texout = texout + "with primitive CM by the maximal order $\\mathcal{O}_K$ of $K$.\n"
        else:
            texout = texout + "with primitive CM by $\\mathcal{O}_K$.\n"
        if hstar == 1:
            texout = texout + "A conjectural model is given in "
        else:
            texout = texout + "Conjectural models are given in "
        if not KW is None:
            if firstexample:
                texout = texout + "Koike-Weng~"
            texout = texout + "\cite[6.1(" + KW + ")]{KoikeWeng} and "
        if firstexample:
            texout = texout + "Lario-Somoza~"
        texout = texout + "\cite[" + LS + "]{LarioSomoza}.\n"
        if hstar == 1:
            texout = texout + "Its invariants are\n"
            texout = texout + "\\begin{itemize}\n"

            (a,b,c) = i[0]
            den2 = (a ^ 3 / b ^ 2).denominator()
            den4 = (c ^ 3 / b ^ 4).denominator()
            den = 1
            for p in (den2 * den4).prime_factors():
                den = den * p ^ (max(my_ceil(den2.valuation(p) / 2), my_ceil(den4.valuation(p) / 4)))
            denKW1 = (b ^ 2 / a^3).denominator()
            denKW2 = (c / a ^ 2).denominator()
            denKWabs = 1
            for p in (denKW1 * denKW2).prime_factors():
                denKWabs = denKWabs * p ^ (max(my_ceil(denKW1.valuation(p) / 3), my_ceil(denKW2.valuation(p) / 2)))
            try:
                if not N.value() % den == 0:
                    print "Inconsistent!"
            except TypeError:
                print "Consistency not checked"
            print "Actual minimal denominator: ", den.factor()

            Ndif = Nprod / Nremark44prod

            print Ndif

            P.<x> = QQ[]
            D = (x ^ 4 + a * x ^ 2 + b * x + c).discriminant()
            den_Delta = lcm([(a ^ 6 / D).denominator(), (b ^ 4 / D).denominator(), (c ^ 3 / D).denominator()])

            texout = texout + "\\item "
            texout = texout + " $i_1 = " + latex_factor_rational(a ^ 6 / D)
            texout = texout + "$, $i_2 = " + latex_factor_rational(a^3 * b ^ 2 / D)
            texout = texout + "$, $i_3 = " + latex_factor_rational(a^4 * c / D)
            texout = texout + "$\n"

            texout = texout + "\\item "
            texout = texout + " $\\denDeltaabs{} = " + latex_factor_integer(den_Delta)
            texout = texout + " \\approx " + str(short_RR(den_Delta)) + "$\n"
            texout = texout + ", \\quad"
            texout = texout + " $\\frac{1}{8}B^{10} \\approx " + str(short_RR(B ^ 10 / 8)) + "$\n"

            texout = texout + "\\item "
            texout = texout + " $j_1' = " + latex_factor_rational(b ^ 2 / a ^ 3)
            texout = texout + "$, $j_2' = " + latex_factor_rational(c / a ^ 2)
            texout = texout + "$\n"

            texout = texout + ", \\quad"
            texout = texout + " $\\denKWabs{} = " + latex_factor_integer(denKWabs)
            texout = texout + " \\approx " + str(short_RR(denKWabs)) + "$\n"

            texout = texout + "\\item "
            texout = texout + " $j_1 = " + latex_factor_rational(a ^ 3 / b ^ 2)
            texout = texout + "$, $j_2 = " + latex_factor_rational(a*c / b ^ 2)
            texout = texout + "$\n"

            texout = texout + ", \\quad"
            texout = texout + " $\\denabs{} = " + (den.factor())._latex_()
            texout = texout + " \\approx " + str(short_RR(den)) + "$\n"
            texout = texout + "\\end{itemize}\n \n "
        elif hstar == 4:
            if firstlongexample:
                texout = texout + "Let $H_{j_1}=H_{\\mathcal{O}_K,1}$ and $\\widehat{H}_{j_1,j_2}=\\widehat{H}_{\mathcal{O}_K, 2}$ be \n"
                texout = texout + "the polynomials as in \\eqref{eq:classpol1}--\\eqref{eq:classpol2}, \n"
                texout = texout + "and let ${H}_{j_1'}$ and $H_{i_1}$ be defined as in \\eqref{eq:classpol1}, \n"
                texout = texout + "but with the invariants $j_1'$ and $i_1$ instead of~$j_1$.\n "
                texout = texout + "These polynomials are numerically approximable with the methods of Koike-Weng~\\cite{KoikeWeng} and Lario-Somoza~\\cite{LarioSomoza} \n "
                texout = texout + "and satisfy "
                texout = texout + "$H_{j_1}(j_1(C))=0$ and $j_2(C) = \\widehat{H}_{j_1,j_2}(j_1(C)) / H_{j_1}'(j_1(C))$ \n"
                texout = texout + "(see~\\cite{GHKRW}). \n"
                texout = texout + "Then the denominators of these polynomials are"
            else:
                texout = texout + "In the notation of Example~8, we have "
            s = K0.automorphisms()
            assert len(s) == 3
            (a,b,c) = i[1]
            a = K0(a)
            b = K0(b)
            c = K0(c)
            i = [i[0]] + [(f(a), f(b), f(c)) for f in s]
            j1 = [(a^3/b^2) for (a,b,c) in i]
            j2 = [(a*c / b ^ 2) for (a, b, c) in i]
            j1prime = [(b ^ 2/ a^3) for (a, b, c) in i]
            j2prime = [(c / a^2) for (a, b, c) in i]
            i1 = [(a^6 / ((x ^ 4 + a * x ^ 2 + b * x + c).discriminant())) for (a,b,c) in i]

            H1 = P(prod([x-j for j in j1]))
            denH1 = H1.denominator()
            H12hat = P(sum([j2[l] * prod([x - j1[m] for m in range(len(j1)) if m != l]) for l in range(len(j1))]))
            denH12hat = H12hat.denominator()
            H1prime =  (prod([x-j for j in j1prime]))
            denH1prime = H1prime.denominator()
            Hi1 =  (prod([x-j for j in i1]))
            denHi1 = Hi1.denominator()
            texout = texout + "\\begin{itemize}\n"
            texout = texout + "\\item $\\mathrm{den}(H_{j_1}) = " + latex_factor_rational(denH1)
            texout = texout + "\\approx " + str(short_RR(denH1))
            texout = texout + "$\n"
            texout = texout + "\\\\ $\\mathrm{den}(\\widehat{H}_{j_1,j_2}) = " + latex_factor_rational(denH12hat)
            texout = texout + "\\approx " + str(short_RR(denH12hat))
            texout = texout + "$\n"
            texout = texout + "\\item $\\mathrm{den}(H_{j_1'}) = " + latex_factor_rational(denH1prime)
            texout = texout + "\\approx " + str(short_RR(denH1prime))
            texout = texout + "$\n"
            texout = texout + "\\item $\\mathrm{den}(H_{i_1}) = " + latex_factor_rational(denHi1)
            texout = texout + "\\approx " + str(short_RR(denHi1))
            texout = texout + "$\n"
            texout = texout + "\\end{itemize}\n \n "
            firstlongexample = False


        texout = texout + "\\begin{itemize}\n"

        texout = texout + "\\item "
        texout = texout + " $N_{" + mu + "} = " + latex_factor_integer(Nprod)
        texout = texout + " \\approx (" + str(short_RR(Nprod.value()^(1/3))) + ")^{3}$\n"

        texout = texout + "\\end{itemize}\n \n \n \\vspace{2mm} \n \n"

        print ""
        firstexample = False
    return texout.replace("+ -", "-")


def compute_bounds():
    """
    This function computes the numbers that appear in Section 10 of [KLS]
    and puts them in a list of tuples (KW, LS, p, i, mu_latex, N, Nprod, Nremark44, Nremark44prod, B).

    Here
     * KW is the example number in Koike-Weng,
     * LS is the section number in Lario-Somoza,
     * p is a defining polynomial of K+,
     * i is a set of tuples that are coefficients (a,b,c) of a Picard curve model,
       where a in QQ is just given as an element of QQ, and a in K+ is given as a tuple
       of coordinates with respect to the basis (1 mod p, x mod p, x^2 mod p) of K+,
     * mu_latex is a TeX representation of mu
     * N would be the gcd of N_mu as mu ranges over a set of appropriate mu values
       IF N_mu were computed with gcd's instead of products
     * Nprod is the gcd of N_mu as mu ranges over a set of appropriate mu values
     * Nremark44 and Nremark44prod are as Nprod and N, but modifying [KLS] using [KLS, Remark 4.4].
     * B is the number B from [KLLNOS]. See sextic_CM_to_KLLNOS_B.

    """
    l = []
    for (KW, LS, p, i) in poly_and_invars:
        K.<alpha> = NumberField(p)
        [mu1], N, Nprod = real_field_to_Picard_primes(K, one_mu=True, remark44=False, output_mu=True)
        [mu2], Nremark44, Nremark44prod = real_field_to_Picard_primes(K, one_mu=True, remark44=True, output_mu=True)
        assert mu1 == mu2
        mu = mu1
        assert radical(N) == radical(Nprod)
        assert radical(Nremark44) == radical(Nremark44prod)
        K1 = QuadraticField(-3)
        P.<x> = K1[]

        K_rel = NumberField(P(p), name='b')
        K = K_rel.absolute_field(names='c')

        B = sextic_CM_to_KLLNOS_B(K)[0]

        l.append((KW, LS, p, i, mu._latex_(), N, Nprod, Nremark44, Nremark44prod, B))
    return l


def short_RR(a):
    """
    Given a real number, returns a short string that represents that real number.    
    """
    a = RR(a)
    if a <= 10:
        return str(a)
    e = (log(a)/log(10)).n().floor()
    a = a / 10^(e-1)
    a = RR(round(a))
    a = a * 10^(e-1)
    return a.str(no_sci=False, skip_zeroes=True, e="\\cdot 10^{") + "}"


def n_from_xamu(x, a, mu, remark44, m=2):
    """
    Returns (b, n), where b is True or False depending on whether
    these are valid x and a values, and if True, then n is the corresponding
    number n. (All as in KLS.)

    if ``remark44'' is true, use the sharper formulas that one gets from Remark 4.4.

    In the input, m has to be a multiple of the number m in the paper. In particular,
    m=2 always suffices.
    """
    if not mu.minpoly().degree() == 3:
        raise ValueError
    t1 = mu.trace()
    assert t1 == -mu.minpoly()[2]
    t2 = (mu^2).trace()
    a1 = mu.minpoly()[1]
    t3 = (mu^3).trace()
    N = mu.norm()
    assert N == - mu.minpoly()[0]
    d, c, b, n, gamma, M = xamu_to_dcbngammaM(x, a, mu, remark44, m)
    assert mu.minpoly()(M) == 0
    if remark44:
        if n > 0 and n in ZZ and b in ZZ and gamma > 0 and n * c in ZZ and gamma in ZZ:
            return True, n
    else:
        if m != 2:
            warnings.warn("what happened to the factor 4? in the paper, we have n = 4*(alpha*gamma-beta*beta_dual)", UserWarning)
        if n > 0 and n in ZZ and b in ZZ and gamma > 0:
            return True, n
    return False, None


def xa_from_mu(mu):
    """
    Returns a list containing all valid x and a pairs.
    """
    t2 = (mu^2).trace()
    l = []
    for x in range(-floor(sqrt(t2)), floor(sqrt(t2))+1):
        # This is inequality eq:boundax (or maybe a weaker version, but that is fine).
        amax = floor((t2-x^2)/2) # yes, it
        for a in range(1, amax+1):
            l.append((x,a))
    return l


def n_from_mu(mu, remark44, m=2):
    # Sets only grow when replacing m by a multiple, so for m<=2 we can use m=2 in the code.
    l = [n_from_xamu(x, a, mu, remark44=remark44, m=m) for (x, a) in xa_from_mu(mu) if xamu_allowed_by_matrices(x, a, mu, remark44=remark44)]
    n_lcm = lcm([n for (b,n) in l if b])
    n_prod = prod([n for (b,n) in l if b])
    return n_lcm, n_prod


def elements_of_bounded_trace_norm(bas, B, bar=None, exclude_rationals=True):
    """
    Returns the set of elements a of <bas>_ZZ with Tr(a*bar(a)) <= B.

    Here bar is complex conjugation and bas is a basis of an order in a CM field
    or totally real field.
    """
    K = Sequence(bas).universe()
    try:
        K = K.number_field()
    except AttributeError:
        pass
    bas = [K(b) for b in bas]
    if bar is None:
        if K.is_totally_real():
            bar = K.hom(K)
        else:
            bar = K.complex_conjugation()
    L = Matrix([[(b*bar(c)).trace() for b in bas] for c in bas])
    Q = QuadraticForm(2*L)
    l = flatten(Q.short_vector_list_up_to_length(B))
    l = [sum([bas[i]*e[i] for i in range(len(bas))]) for e in l]
    if exclude_rationals:
        l = [a for a in l if not a in QQ]
    return l


def real_field_to_mu(F, B=None):
    """
    Given a real cubic field F, returns a set of elements mu of ZZ + 2OF.
    In fact, it returns the set of elements with with Tr(a*bar(a)) <= B.
    If B is None, takes the smallest B for which the output is non-empty.
    """
    OF = F.maximal_order()
    O = F.order([1] + [2*b for b in OF.basis()])
    bas = O.basis()
    if B is None:
        B = 1
        while True:
            l = elements_of_bounded_trace_norm(bas, B)
            if len(l) >= 20:
                break
            B = B + 1
    else:
        l = elements_of_bounded_trace_norm(bas, B)
        if len(l) == 0:
            raise ValueError, "Bound too low"
    return l


def basis_in_mu(mu):
    """
    For mu totally real non-rational in a sextic CM field containing
    a square root r of -3, this function returns
    a sequence giving the elements of a basis of ZZ + 2*O_K
    in terms of the basis 1, mu, mu^2, r, r*mu, r*mu^2 of K.
    """
    K = mu.parent()
    if K.degree() == 3:
        P.<x> = K[]
        Krel.<b> = NumberField(x^2+3)
        K.<c> = Krel.absolute_field()
        s = K.structure()
        mu = s[1](mu)
        r = s[1](b)
    elif K.degree() == 6:
        r = K(-3).sqrt()
    else:
        raise ValueError
    O = K.order([2*b for b in K.maximal_order().basis()])
    B = [list(K(b)) for b in O.basis()]
    C = Matrix([list(mu^(ZZ(i%3))*r^(ZZ(floor(i/3)))) for i in range(6)]).transpose()
    return [C.inverse()*vector(b) for b in B]


K1.<sqm3> = QuadraticField(-3)     # K1    = QQ(sqrt(-3))
Msqm3 = sqm3 * identity_matrix(3)  # Msqm3 = sqrt(-3) * Id_{3x3}.


def xamu_to_dcbngammaM(x, a, mu, remark44, m=2):
    """
    Given x, a, mu as in [KLS], return a tuple (d, c, b, n, gamma, M) as in [KLS].
    """
    if not mu.minpoly().degree() == 3:
        raise ValueError
    t1 = mu.trace()
    assert t1 == -mu.minpoly()[2]
    t2 = (mu ^ 2).trace()
    a1 = mu.minpoly()[1]
    t3 = (mu ^ 3).trace()
    N = mu.norm()
    assert N == - mu.minpoly()[0]
    if remark44:
        d = t1
        c = -(a1 + x ^ 2 + a) / 2
        b = (N - (x ^ 3 + (-t1) * x ^ 2 + x * a + a1 * x + (-t1) * a)) / 2 # ????
        #b = (t3-(x^3+3*a*x+3*c*d+d^3))/3 # ????
        alpha = m * a
        beta = m * b
        gamma = (alpha * c + beta * d + x * beta) / 2
        mprime = m / gcd(m, alpha * gamma - beta ^ 2)
        n = mprime * (alpha * gamma - beta ^ 2)
        #        gammadivm = a * c + b * d + x * b
        #        gammadivm = (a * c + b * d + x * b) / 2
        gammadivm = gamma / m
        M = Matrix([[x, a, b], [1, -x, c], [0, 2, d]])
        assert mu.minpoly()(M) == 0
        #        n = m ^ 2 * (a * gammadivm - b ^ 2)
    else:
        d = t1 - x
        c = (t2 - (x ^ 2 + 2 * a + d ^ 2)) / 2
        assert c == -(a1 + x ^ 2 + a - t1 * x)
        b = (t3 - (x ^ 3 + 3 * a * x + 3 * c * d + d ^ 3)) / 3
        assert b == N - (x ^ 3 - t1 * x ^ 2 + 2 * x * a + a1 * x - t1 * a)
        alpha = m * a
        beta = m * b
        gamma = (alpha * c + beta * d)
        mprime = m / gcd(m, alpha * gamma - beta ^ 2)
        n = mprime * (alpha * gamma - beta ^ 2)
        #        gammadivm = a*c+b*d
        M = Matrix([[x, a, b], [1, 0, c], [0, 1, d]])
        assert mu.minpoly()(M) == 0
        if m != 2:
            warnings.warn("what happened to the factor 4? in the paper, we have n = 4*(alpha*gamma-beta*beta_dual)",
                          UserWarning)
    assert mu.minpoly()(M) == 0
    return d, c, b, n, gamma, M


def xamu_to_matrix_M(x, a, mu, remark44):
    """
    Given x, a, mu as in [KLS], return the matrix M as in [KLS].
    """
    return xamu_to_dcbngammaM(x, a, mu, remark44)[-1]


def xamu_to_matrices(x, a, mu, remark44):
    """
    Given x, a, mu as in [KLS], return iota(alpha) as alpha ranges over a basis
    of ZZ + 2*O_K.
    """
    M = xamu_to_matrix_M(x, a, mu, remark44=remark44)
    bas = basis_in_mu(mu)
    l = []
    for b in bas:
        N = sum([b[i]*M^(ZZ(i%3))*Msqm3^(ZZ(floor(i/3))) for i in range(6)])
        l.append(N)
    if verbosity > 3:
        print x, a, mu, remark44, l
    return l


def xamu_allowed_by_matrices(x, a, mu, remark44):
    """
    Given x, a, mu as in [KLS], return True or False according
    to whether all matrices iota(alpha) as alpha ranges over a basis
    of ZZ + 2*O_K satisfy the condition that their entries are in 1/n*ZZ[zeta_3]
    with top row in ZZ[zeta_3].
    
    """
    b, n = n_from_xamu(x, a, mu, remark44=remark44)
    if not b:
        return False
    for N in xamu_to_matrices(x, a, mu, remark44=remark44):
            r = N[0]
            for e in r:
                l = list(e)
                if not (2*l[0] in ZZ and l[0]+l[1] in ZZ): # test whether the entry e is in 2Z[zeta_3] = 2Z + 2zeta_3Z = 2Z + (sqrt3-1)Z.
                    return False
            for r in N[1:]:
                for e in r:
                    l = list(e)
                    if not n*2*l[0] in ZZ and n*(l[0]+l[1]) in ZZ:
                        return False
    return True


def latex_factor_integer(x):
    """
    Returns a LaTeX representation of the factorization of an integer x.
    """
    try:
        x = x.value()
    except AttributeError:
        pass
    if not x in ZZ:
        if x^3 in ZZ:
            return "(" + latex_factor_integer(ZZ(x^3)) + ")^{\\frac{1}{2}}"
        return x._latex_()
    if x < 0:
        return "-" + latex_factor_integer(-x)
    a, b = ZZ(x).perfect_power()
    if b == 1 or a.is_prime():
        return x.factor()._latex_()
    return "(" + a.factor()._latex_() + ")^{" + str(b) + "}"


def latex_factor_rational(x):
    """
    Returns a LaTeX representation of the factorization of a rational number x.
    """
    try:
        x = x.value()
    except AttributeError:
        pass
    d = QQ(x).denominator()
    if d == 1:
        return latex_factor_integer(x)
    return "\\frac{" + latex_factor_integer(d*x) + "}{" + latex_factor_integer(d) + "}"


def latex_factor_polynomial(poly):
    """
    Returns a LaTeX representation of the factorization of a polynomial,
    with factored denominator.
    """
    P.<x> = QQ[]
    poly = P(poly)
    F = poly.factor()
    F = [p ^ e for (p, e) in F if p.degree() > 0]
    F = [f * f.denominator() for f in F]
    F = [f / gcd(list(f)) for f in F]
    d = prod([f.lc() for f in F]) / poly.lc()
    dd = d.denominator()
    d = d * dd
    F[0] = F[0] * dd
    out = "{\\normalsize (" + latex_factor_rational(d) + ")^{-1}}"
    for f in F:
        out = out + " \\cdot(" + f._latex_() + ")"
    return out
